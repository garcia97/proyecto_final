
package Clases;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Willian_Adalberto_Hernandez_Aquino {
    
    public static void main(String[] args) {

        try {
            BufferedReader leer = new BufferedReader(new InputStreamReader(System.in));
            Ejercicio2 examen2 = new Ejercicio2();
            
            // Variables
            String nombre;
            double renta;
            double afp;
            double isss;
            double total;
            double total1;
            double total2;
            double total3;
            // Instancia de los metodos
            examen2.sueldoConHorasED();
            examen2.sueldoConHtriple();
            examen2.sueldoSinHorasE();
            
            System.out.println("Ingrese el nombre del empleado: ");
            nombre = String.format(leer.readLine());
            System.out.println("Ingrese el valor de la hora: $");
            examen2.setPrecioHn(Double.parseDouble(leer.readLine()));
            System.out.println("Ingrese horas trabajadas durante el mes: ");
            examen2.sethTMes(Double.parseDouble(leer.readLine()));
            System.out.println("================================================");
            if (examen2.gethTMes() > 50 && examen2.gethTMes() <= 58 ) {
                renta = examen2.sueldoConHorasED() * 0.10;
                afp = examen2.sueldoConHorasED() * 0.075;
                isss = examen2.sueldoConHorasED() * 0.07;
                total = (renta + afp + isss);
                total1 = examen2.sueldoConHorasED() - total;
                System.out.println("El descuento de ISR es: \n$"+renta);
                System.out.println("El descuento de AFP es: \n$"+afp);
                System.out.println("El descuento de ISSS es: \n$"+isss);
                System.out.println("=================================================");
                System.out.println("El pago sin descuento es: $"+examen2.sueldoConHorasED());
                System.out.println("El pago mensual con horas al doble de: "+nombre +"\ndespues de descuentos es: $"+total1);
            }
            
            else if (examen2.gethTMes() > 58) {
                renta = examen2.sueldoConHtriple()* 0.10;
                afp = examen2.sueldoConHtriple() * 0.075;
                isss = examen2.sueldoConHtriple() * 0.07;
                total = (renta + afp + isss);
                total2 = examen2.sueldoConHtriple() - total;
                System.out.println("El descuento de ISR es: \n$"+renta);
                System.out.println("El descuento de AFP es: \n$"+afp);
                System.out.println("El descuento de ISSS es: \n$"+isss);
                System.out.println("=================================================");
                System.out.println("El pago sin descuentos es: $"+examen2.sueldoConHtriple());
                System.out.println("El pago mensual con horas al triple de: "+nombre +"\ndespues de descuentos es: $"+total2);
            }
            else if (examen2.gethTMes() <= 50) {
                renta = examen2.sueldoSinHorasE()* 0.10;
                afp = examen2.sueldoSinHorasE() * 0.075;
                isss = examen2.sueldoSinHorasE() * 0.07;
                total = (renta + afp + isss);
                total3 = examen2.sueldoSinHorasE() - total;
                System.out.println("El descuento de ISR es: \n$"+renta);
                System.out.println("El descuento de AFP es: \n$"+afp);
                System.out.println("El descuento de ISSS es: \n$"+isss);
                System.out.println("========================================================");
                System.out.println("El pago sin descuentos es: "+examen2.sueldoSinHorasE());
                System.out.println("El pago mensual sin horas extras despues de descuentos de: "+nombre +" \nes: $"+total3);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        
    }
}

