
package Clases;

public class Ejercicio2 {
    
    double precioHn;
    double hTMes;

    // Constructor vacio
    Ejercicio2() {
        
    }
    
    public double getPrecioHn() {
        return precioHn;
    }

    public void setPrecioHn(double precioHn) {
        this.precioHn = precioHn;
    }

    public double gethTMes() {
        return hTMes;
    }

    public void sethTMes(double hTMes) {
        this.hTMes = hTMes;
    }
    
    
    //Metodos para calcular sueldo
    public double sueldoSinHorasE(){ //Calcula salario sin horas extras
    double pago = this.hTMes * this.precioHn;
    return pago;
    }
    
    public double sueldoConHorasED(){ //Calcula salario con horas extras pagadas al doble
    double pago = (50 * 8) + (this.hTMes-50) * (8*2);
    return pago;
    }
    
    public double sueldoConHtriple(){ //Calcula salario con horas pagadas al doble y triple
    double pago = (50 * 8) + (8 * 8 * 2) + (this.hTMes - 58) * (8*3);
    return pago;
    }
}
